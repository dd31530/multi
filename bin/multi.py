#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
+----------------------------------------------------------------------------+
| multi_module:  a module for multiprocessing                                |
+---------+----------+----------+--------------------------------------------+
| version | datecode | author   | history                                    |
+---------+----------+----------+--------------------------------------------+
| 1.0     | 181127   | DD       | creation                                   |
| 1.1     | 190925   | DD       | python3 adaptation                         |
+---------+----------+----------+--------------------------------------------+
'''

VERSION = 'v1.1_190925'

##############################################################################
# external modules
##############################################################################


from multiprocessing import Process, active_children
from time import sleep
import os
import sys


##############################################################################
# local modules
##############################################################################


from logUtils import Logger
from messagesUtils import header, banner, footer
from pathUtils import currentPath


##############################################################################
# classes 
##############################################################################


class template_class():
   pass


##############################################################################
# functions 
##############################################################################


def hide_cursor():
   sys.stdout.write("\033[?25l")
   sys.stdout.flush()
    

def show_cursor():
   sys.stdout.write("\033[?25h")
   sys.stdout.flush()
    

def skip_line(n):
   sys.stdout.write('\033[%iE' % n)
   sys.stdout.flush()
    

def clear_screen():
   sys.stdout.write("\033[2J")
   sys.stdout.flush()
    

def clear_line(line):
   sys.stdout.write('\033[%i;1H' % line)
   sys.stdout.write("\033[2K")
   sys.stdout.flush()
    

def write_xy(x,y,w):
   sys.stdout.write('\033[%i;%iH%s  ' % (y,x,w))
   sys.stdout.flush()


def frame(x1,x2,y1,y2):
   for lig in range (y1,y2+1,1):
      for col in range (x1,x2+1,1):
         sys.stdout.write(u"\u001b[%i;%iH" % (lig,col))
         if (lig == y1):      
            if (col == x1):
               sys.stdout.write(u"\u250c") # asg
            elif (col == x2):      
               sys.stdout.write(u"\u2510") # asd
            else:      
               sys.stdout.write(u"\u2500") # h
         elif (lig == y2):      
            if (col == x1):
               sys.stdout.write(u"\u2514") # aig
            elif (col == x2):      
               sys.stdout.write(u"\u2518") # aid
            else:      
               sys.stdout.write(u"\u2500") # h
         elif (col == x1) or (col == x2):
            sys.stdout.write(u"\u2502")    # v
         else:
            continue


def info(title, offset):
   write_xy(offset,2,title + ':')
   write_xy(offset,4,'parent pid:  %i' % os.getppid())
   write_xy(offset,5,'process id:  %i' % os.getpid())


def f(name, t, off):
   frame(off-2, off+24, 1, 9)
   info(name, off)
   while t >= 0:
      write_xy(off,7,'execution:  %i' % t)
      sleep(1)
      t -= 1 
   write_xy(off,8,'ended.')
   return 'ended.'


def main():
   clear_screen()
   hide_cursor()
   info('main', 8)
   write_xy(8,8,'launching processes ...')

   proc_args = {'Process-1': (20, 38),
                'Process-2': (8, 68),
                'Process-3': (12, 98)}

   processes = {}

   results = dict()
   for proc_name in sorted(proc_args.keys()):
      (t, p) = proc_args[proc_name]
      processes[proc_name] = Process(target=f, args=(proc_name, t, p,))
      results[proc_name] = processes[proc_name].start()
      sleep(0.5)

   write_xy(8,8,'waiting end processes ...')

   while True:
      clear_line(12)
      status = active_children()
      if len(status) == 0: break
      write_xy(8,12,"remaining:  " + str(status))
      sleep(1)

   for proc_name in sorted(proc_args.keys()):
      processes[proc_name].join()

   write_xy(8,8,'all processes terminated.')
   skip_line(4)
   write_xy(8,12,"results:  %s\n\n" % results)
   show_cursor()


##############################################################################
# main
##############################################################################


if __name__ == '__main__':

   #header()
   #banner("Execution...")
   status=main()
   #footer(status)


##############################################################################
# eof
##############################################################################
